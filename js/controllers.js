/*
    Document   : controllers.js
    Created on : 20.11.2013, 11:14:28
    Author     : Ivan
    Description: Controllers for "Service monitoring" application
*/
//'use strict';

var ServiceMonitoringApp = angular.module('ServiceMonitoringApp', []);

//function ServiceMonitoringCtrl ($scope, $http) {
ServiceMonitoringApp.controller('ServiceMonitoringCtrl', function($scope, $http) {

	//Периодичность запроса данных о мониторинге сервисов 
	var updateDataInterval = 10000;

	$scope.services = [];
	$scope.regions = [];
	var error = false;

	// Load regions
	$http.get('res/regions.json').success(function(ans) {
		$scope.regions = ans;		
	});
	

	//Стартовый (однократный) запрос данных
	$http.get('res/data.json').success(function(ans) {

		//var i = 0;

		var data  = [];
		if (ans['res'] == 1) {

			data = ans['data'];	

			//alert(ans['lv_1'].length); // 3

			for (var region in data)
			{
				//var regData = region; //[]
				
				alert(regData.length); // lv_1 => 4 , ru_1 => 4, all_1 => 5
				/*
				for(var i = 0; i < regData.length; i++)
				{
					for(var param in regData[i])
					{
						alert(param[0]);
					}
				}

				/*
				for (var service in cyrRegion)
				{
					var ser = service; // []
					alert(ser);
					//$scope.services[i].Region = cyrRegion;
					/*
					for (var param in service)
					{
						$scope.services[i].param = param;
						alert($scope.services[i]);
					}
					*/	
				//}

				
				/*
				alert("Region: " + region + " value: " +data[redion] );
				$scope.services = [];
				*/
			}

			//i++;

		}


	/*
 	{ "region": "ru_1", "service_type": "Nas", "name": "Authorize 1",
 		"falseAuthorizeCount": 0,
 		"trueAuthorizeCount": 0,
 		"totalRequestCount": 1,
 		"state": "None"
     },
	*/
		  
	//$http.get('res/data.json').success(function(data) { $scope.smData = data;  alert($scope.smData) });
	
	/* test JSON from Evil
	var str = $scope.services; ---- use convert to string
	var user = !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(
	str.replace(/"(\\.|[^"\\])*"/g, ''))) &&
	  eval('(' + str + ')'); 
	
	alert(user); ---- user : bool	
	*/

	/* js-example
			var res = JSON.parse(data, function(key, value) {
		  		if (key == 'res') return value;
			});
	*/
		
	//	alert(angular.fromJson($scope.services)); // return Object

	});

	//Выбранный регион при старте приложения - "Все регионы"
	$scope.selectedRegion = 'all_1';

	
	//Отслеживание изменения данных в свойстве 'selectedRegion' (выбранный регион)
	$scope.$watch('selectedRegion', function()
	{
		//Пересчет суммы значений для каждого столбца в таблицах
		dataRecalc();		
	});

	//Отслеживание изменения данных в массиве 'services'
	$scope.$watch('services', function()
	{
		//Пересчет суммы значений для каждого столбца в таблицах
		dataRecalc();		
	});

	//Чтение json-данных
	var updateData = function(){
		$http.get('res/test.json').success(function(data) {
			$scope.services = data;
		});
	};

	//Циклический запуск функции 'updateData'
	/*
	var timer = setInterval(function() {
		$scope.$apply(updateData);
	}, updateDataInterval);
	*/

	//Пересчет суммы значений для каждого столбца в таблицах
	function dataRecalc()
	{
		var param1 = 0;
		var param2 = 0;
		var param3 = 0;

		//
		for (var i = 0; i < $scope.services.length; i++)	//forEach
		{
			switch($scope.services[i].service_type)
			{
					case "Authorize":
						if($scope.services[i].region == $scope.selectedRegion)
						{
							param1 += $scope.services[i].falseAuthorizeCount;
							param2 += $scope.services[i].trueAuthorizeCount;
							param3 += $scope.services[i].totalRequestCount;
						}
						break;
			}
			
			//Переназначение параметра 'state' из "нашего" типа в тип "bootstrap"
			// отвечающего за цвет фона строки (состояние сервиса)
			/*
			switch($scope.services[i].state)
			{
				case "None": $scope.services[i].state = "";  break;
				case "Free": $scope.services[i].state = "success";  break;
				case "Stable": $scope.services[i].state = "info";  break;
				case "Busy": $scope.services[i].state = "warning";  break;
				case "Error": $scope.services[i].state = "error";  break;
			}
			*/

		}

		$scope.falseAuthorizeCount = param1;
		$scope.trueAuthorizeCount = param2;
		$scope.totalRequestCount = param3;
	}

	/*
	// ------------------------------------------------------------------------- 
	$scope.services = mas.concat(serv);
	$http.get('../data.json').success(function(data) { $scope.smData = data; });
	// -------------------------------------------------------------------------
	*/
});
